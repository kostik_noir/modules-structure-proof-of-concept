import * as activeMetricType from './active-metric-type/active-metric-type';
import * as baseRange from './base-range/base-range';
import * as comparisonRange from './comparison-range/comparison-range';
import * as comparisonSeriesConnector from './comparison-series/connector';
import * as comparisonSeries from './comparison-series/comparison-series';
import * as comparisonSeriesRequest from './comparison-series/request';
import * as liveMode from './live-mode/live-mode';

export const state = {
  [activeMetricType.mountPoint]: activeMetricType.initialState,
  [baseRange.mountPoint]: baseRange.initialState,
  [comparisonRange.mountPoint]: comparisonRange.initialState,
  [comparisonSeries.mountPoint]: comparisonSeries.initialState,
  [comparisonSeriesRequest.mountPoint]: comparisonSeriesRequest.initialState,
  [liveMode.mountPoint]: liveMode.initialState
};

export const middlewares = [
  comparisonSeriesConnector.middleware
];

export const mutations = Object.assign(
  {},
  activeMetricType.mutations,
  baseRange.mutations,
  comparisonRange.mutations,
  comparisonSeries.mutations,
  comparisonSeriesRequest.mutations
);
