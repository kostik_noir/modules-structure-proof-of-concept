import { initialState } from './comparison-range';

describe('comparisonRange', () => {
  it('should provides correct initial state', () => {
    const { start, end } = initialState;
    expect(start).toBeDefined();
    expect(end).toBeDefined();
  });
});

