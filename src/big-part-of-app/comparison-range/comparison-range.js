import { dataFactory } from '../../utils/data-factory';

const prefix = 'comparisonRange';

export const mountPoint = 'comparisonRange';

export const initialState = {
  start: new Date(),
  end: new Date()
};

export const {
  CHANGED,
  mutations,
  change,
  getValue
} = dataFactory({ prefix, mountPoint });
