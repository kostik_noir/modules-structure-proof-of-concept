import {
  initialState
} from './active-metric-type';

describe('activeMetricType', () => {
  it('should provides correct initial state', () => {
    expect(initialState).toEqual('pagesview');
  });
});
