import { dataFactory } from '../../utils/data-factory';

const prefix = 'activeMetricType';

export const mountPoint = 'activeMetricType';

export const initialState = 'pagesview';

export const {
  CHANGED,
  mutations,
  change,
  getValue
} = dataFactory({ prefix, mountPoint });
