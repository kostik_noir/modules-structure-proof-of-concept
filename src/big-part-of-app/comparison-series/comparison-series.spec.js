import { initialState } from './comparison-series';

describe('comparisonSeries', () => {
  it('should provides correct initial state', () => {
    expect(initialState).toEqual({});
  });
});
