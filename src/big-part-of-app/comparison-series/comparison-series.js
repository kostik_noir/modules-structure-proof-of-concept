import { dataFactory } from '../../utils/data-factory';

const prefix = 'comparisonSeries';

export const mountPoint = 'comparisonSeries';

export const initialState = {};

export const {
  CHANGED,
  mutations,
  change,
  getValue
} = dataFactory({ prefix, mountPoint });
