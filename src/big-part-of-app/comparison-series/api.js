import { defer } from '../../utils/defer';

/* eslint-disable no-unused-vars */
export function fetch (metricType, baseRange, comparisonRange) {
  const deferred = defer();
  // TODO: send request, unwrap response, parse response
  return deferred.promise;
}
