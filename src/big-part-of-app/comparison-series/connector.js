import { CHANGED as BASE_RANGE_CHANGED } from '../base-range/base-range';
import { CHANGED as COMPARISON_RANGE_CHANGED } from '../comparison-range/comparison-range';
import { CHANGED as METRIC_TYPE_CHANGED } from '../active-metric-type/active-metric-type';
import { fetch } from './request';

const types = [
  BASE_RANGE_CHANGED,
  COMPARISON_RANGE_CHANGED,
  METRIC_TYPE_CHANGED
];

export const middleware = {
  onMutation ({ type }, _, store) {
    if (types.indexOf(type) === -1) {
      return;
    }
    fetch(store);
  }
};
