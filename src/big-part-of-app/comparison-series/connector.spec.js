import { CHANGED as BASE_RANGE_CHANGED } from '../base-range/base-range';
import { CHANGED as COMPARISON_RANGE_CHANGED } from '../comparison-range/comparison-range';
import { CHANGED as METRIC_TYPE_CHANGED } from '../active-metric-type/active-metric-type';
import * as request from './request';

import { middleware } from './connector';

const { onMutation } = middleware;

describe('comparison series connector', () => {
  let store;

  beforeEach(() => {
    request.fetch = jasmine.createSpy('fetch');

    store = {
      dispatch: jasmine.createSpy('dispatch')
    };
  });

  it('should initialize fetch of data', () => {
    const types = [
      BASE_RANGE_CHANGED,
      COMPARISON_RANGE_CHANGED,
      METRIC_TYPE_CHANGED
    ];

    types.forEach((type) => {
      onMutation({ type }, null, store);
      expect(request.fetch).toHaveBeenCalledWith(store);
      request.fetch.calls.reset();
    });
  });
});
