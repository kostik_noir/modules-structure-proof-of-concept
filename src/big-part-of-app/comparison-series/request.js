import { getActiveMetricType } from '../active-metric-type/active-metric-type';
import { getValue as getBaseRange } from '../base-range/base-range';
import { getValue as getComparisonRange } from '../comparison-range/comparison-range';
import { fetch as fetchData } from './api';
import { change as changeComparisonSeries } from './comparison-series';

const prefix = 'comparisonSeriesRequest';

export const mountPoint = 'comparisonSeriesRequest';

export const FETCH = `${prefix}/FETCH`;
export const CANCELED = `${prefix}/CANCELED`;
export const STARTED = `${prefix}/STARTED`;
export const COMPLETED = `${prefix}/COMPLETED`;
export const FAILED = `${prefix}/FAILED`;

export const initialState = {
  started: false,
  error: false
};

// --------------------------
// mutations
// --------------------------
export const mutations = {
  [FETCH]: () => {
  },
  [CANCELED]: () => {
  },
  [STARTED]: (state) => {
    state[mountPoint] = {
      started: true,
      error: false
    };
  },
  [COMPLETED]: (state) => {
    state[mountPoint] = {
      started: false,
      error: false
    };
  },
  [FAILED]: (state) => {
    state[mountPoint] = {
      started: false,
      error: true
    };
  },
  [CANCELED]: (state) => {
    state[mountPoint] = {
      started: false,
      error: false
    };
  }
};

// --------------------------
// public actions
// --------------------------
export function fetch ({ dispatch }) {
  dispatch(FETCH);
}

export function cancel ({ dispatch }) {
  dispatch(CANCELED);
}

// --------------------------
// getters
// --------------------------
export function started (state) {
  return state[mountPoint].started === true;
}

export function hasError (state) {
  return state[mountPoint].error === true;
}

// --------------------------
// behaviour
// --------------------------
export const middleware = {
  onMutation ({ type }, _, store) {
    switch (type) {
      case FETCH:
        sendRequest(store);
        break;
      case CANCELED:
        cancelRequest();
        break;
      // no default
    }
  }
};

let request = null;

let requestOpts = {
  metricType: null,
  baseRange: null,
  comparisonRange: null
};

function sendRequest (store) {
  const { state, dispatch } = store;

  const newOpts = {
    metricType: getActiveMetricType(state),
    baseRange: getBaseRange(state),
    comparisonRange: getComparisonRange(state)
  };

  if (!needsNewRequest(request, requestOpts, newOpts)) {
    return;
  }

  requestOpts = newOpts;
  cancelRequest();

  if (!started(state)) {
    dispatch(STARTED);
  }

  request = fetchData(requestOpts.metricType, requestOpts.baseRange, requestOpts.comparisonRange)
    .then((data) => {
      request = null;
      changeComparisonSeries(store, data);
      dispatch(COMPLETED);
    })
    .catch(() => {
      request = null;
      dispatch(FAILED);
    });
}

function cancelRequest () {
  if (request === null) {
    return;
  }
  request.cancel();
  request = null;
}

function needsNewRequest (request, prevOpts, newOpts) {
  if (request === null) {
    return true;
  }
  return prevOpts.metricType !== newOpts.metricType ||
    prevOpts.baseRange !== newOpts.baseRange ||
    prevOpts.comparisonRange !== newOpts.comparisonRange;
}
