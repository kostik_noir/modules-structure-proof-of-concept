import { initialState } from './live-mode';

describe('liveMode', () => {
  it('should provides correct initial state', () => {
    expect(initialState).toEqual(false);
  });
});
