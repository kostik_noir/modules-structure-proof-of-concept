import { dataFactory } from '../../utils/data-factory';

const prefix = 'liveMode';

export const mountPoint = 'liveMode';

export const initialState = false;

export const {
  CHANGED,
  mutations,
  change,
  getValue
} = dataFactory({ prefix, mountPoint });
