import { initialState } from './base-range';

describe('baseRange', () => {
  it('should provides correct initial state', () => {
    const { start, end } = initialState;
    expect(start).toBeDefined();
    expect(end).toBeDefined();
  });
});
