import { dataFactory } from '../../utils/data-factory';

const prefix = 'baseRange';
export const mountPoint = 'baseRange';

export const initialState = {
  start: new Date(),
  end: new Date()
};

export const {
  CHANGED,
  mutations,
  change,
  getValue
} = dataFactory({ prefix, mountPoint });
