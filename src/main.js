import Vuex from 'vuex';

import * as bigPartOfApp from './big-part-of-app';
import * as anotherBigPartOfApp from './another-big-part-off-app';

const state = {};

const modules = {
  bigPartOfApp: {
    state: bigPartOfApp.state,
    mutations: bigPartOfApp.mutations
  },
  anotherBigPartOfApp: {
    state: anotherBigPartOfApp.state,
    mutations: anotherBigPartOfApp.mutations
  }
};

const mutations = {};

const middlewares = [].concat(
  bigPartOfApp.middlewares,
  anotherBigPartOfApp.middlewares
);

export const store = new Vuex.Store({
  state,
  modules,
  mutations,
  middlewares
});
