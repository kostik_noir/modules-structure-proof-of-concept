import Promise from 'promise-polyfill';

export function defer () {
  let _resolveFn;
  let _rejectFn;
  let cancelled = false;

  const promise = new Promise((rejectFn, resolveFn) => {
    _resolveFn = resolveFn;
    _rejectFn = rejectFn;
  });

  promise.cancel = () => {
    cancelled = true;
  };

  const resolve = (...args) => {
    if (cancelled) {
      return;
    }
    _resolveFn.apply(this, args);
  };

  const reject = (...args) => {
    if (cancelled) {
      return;
    }
    _rejectFn.apply(this, args);
  };

  return {
    promise,
    resolve,
    reject
  };
}
