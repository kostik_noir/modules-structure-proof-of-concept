export function dataFactory ({ prefix, mountPoint }) {
  const CHANGED = `${prefix}/CHANGED`;

  return {
    CHANGED,
    mutations () {
      return {
        [CHANGED] (state, value) {
          state[mountPoint] = value;
        }
      };
    },
    change ({ dispatch }, value) {
      dispatch(CHANGED, value);
    },
    getValue (state) {
      return state[mountPoint];
    }
  };
}
