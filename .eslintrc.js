module.exports = {
  extends: 'airbnb/base',
  env: {
    browser: true,
    node: true,
    jasmine: true
  },
  rules: {
    'comma-dangle': ['error', 'never'],
    'space-before-function-paren': ['error', 'always'],
    'no-param-reassign': ['error', { props: false }],
    'no-shadow': [0],
    'no-use-before-define': ['error', 'nofunc']
  }
};
