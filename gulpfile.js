var path = require('path');
var gulp = require('gulp');
var karma = require('karma');
var browserify = require('browserify');
var babelify = require('babelify');
var eslint = require('gulp-eslint');

// --------------------------
// config
// --------------------------
var dirs = {
  cwd: process.cwd(),
  src: path.join(process.cwd(), 'src'),
  dest: {
    dev: path.join(process.cwd(), '.tmp'),
    release: path.join(process.cwd(), 'dist')
  }
};

var browserifyCfg = {
  opts: {
    extensions: ['.js', '.json']
  },
  plugins: [],
  transforms: [
    babelify
  ]
};

var env = {
  dev: {
    NODE_ENV: 'development'
  },
  release: {
    _: 'purge',
    NODE_ENV: 'production'
  }
};

var cfg = {
  karma: {
    configFile: path.join(process.cwd(), 'karma.conf.js'),
    browserify: {
      debug: true,
      plugin: [].concat(browserifyCfg.plugins),
      transform: browserifyCfg.transforms,
      extensions: browserifyCfg.extensions
    }
  },
  js: {
    src: [
      path.join(dirs.src, 'main.js')
    ],
    dest: {
      fileName: 'build.js',
      dev: dirs.dest.dev,
      release: dirs.dest.release
    }
  }
};

// run tests only one time
gulp.task('test', function (done) {
  new karma.Server(cfg.karma, done).start();
});

// run tests, watch changes, run tests again etc
gulp.task('test:watch', function (done) {
  // TODO: watchify
  var config = Object.assign({}, cfg.karma, {
    singleRun: false
  });
  new karma.Server(config, done).start();
});

// lint js code
gulp.task('lint', function () {
  return gulp.src('src/**/*.{js,vue}')
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});
