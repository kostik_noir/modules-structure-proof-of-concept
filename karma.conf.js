var path = require('path');

module.exports = function (config) {
  config.set({
    browsers: ['PhantomJS'],
    frameworks: ['browserify', 'jasmine'],
    files: ['src/**/*.spec.js'],
    reporters: ['dots'],
    preprocessors: {
      'src/**/*.js': ['browserify']
    },
    singleRun: true
  })
};
